import unittest

from main import Complex


class ComplexTest(unittest.TestCase):
    def setUp(self):
        self.a = Complex(9)
        self.b = Complex(15, 5)
        self.c = Complex('2i - 6')

    def test_params(self):
        """
        Проверяет правильную передачу параметров.
        """

        self.assertEqual(self.a.base, 9)
        self.assertEqual(self.a.postfix, 0)

        self.assertEqual(self.b.base, 15)
        self.assertEqual(self.b.postfix, 5)

        self.assertEqual(self.c.base, 2)
        self.assertEqual(self.c.postfix, -6)

        g = Complex('157i + 16')
        self.assertEqual(g.base, 157)
        self.assertEqual(g.postfix, 16)

    def test_addition(self):
        """
        Проверяет сложение.
        """

        self.assertEqual(self.a.postfix, 0)
        self.a += 6
        self.assertEqual(self.a.postfix, 6)

        self.assertEqual(self.c.postfix, -6)
        self.c += 15
        self.assertEqual(self.c.postfix, 9)

        self.a += self.c
        self.assertEqual(self.a.base, 11)
        self.assertEqual(self.a.postfix, 15)

    def test_substraction(self):
        """
        Проверяет вычитание.
        """

        self.assertEqual(self.a.postfix, 0)
        self.a -= 6
        self.assertEqual(self.a.postfix, -6)

        self.assertEqual(self.c.postfix, -6)
        self.c -= 15
        self.assertEqual(self.c.postfix, -21)

        self.a -= self.c
        self.assertEqual(self.a.base, 7)
        self.assertEqual(self.a.postfix, 15)

    def test_multiplication(self):
        """
        Проверяет умножение.
        """

        self.assertEqual(self.a.postfix, 0)
        self.a += 1
        self.a *= -6
        self.assertEqual(self.a.postfix, -6)

        self.assertEqual(self.c.postfix, -6)
        self.c *= 5
        self.assertEqual(self.c.postfix, -30)

        base = self.a.base * self.c.postfix + self.a.postfix * self.c.base
        postfix = -1 * (self.a.base * self.c.base) + self.a.postfix * self.c.postfix
        self.a *= self.c
        self.assertEqual(self.a.base, base)
        self.assertEqual(self.a.postfix, postfix)

        g = Complex(5)
        e = Complex(10)
        self.assertEqual(e*g, g.base * e.base * -1)

    def test_failure(self):
        """
        Проверяет вызов ошибки в непредвиденных ситуациях.
        """

        with self.assertRaises(TypeError):
            self.a += "Hello"

    def test_str(self):
        """
        Проверяет создание строки для вывода.
        """

        def str_gen(complex: Complex):
            if complex.postfix:
                return f'{complex.base}i + ({complex.postfix})'
            return f'{complex.base}i'

        self.assertEqual(str(self.a), str_gen(self.a))
        self.assertEqual(str(self.b), str_gen(self.b))
        self.assertEqual(str(self.c), str_gen(self.c))
