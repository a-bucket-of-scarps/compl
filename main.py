from copy import copy


NUMBERS = '1234567890'
I_SYMBOL = 'i'


class Complex:

    def _parse_str_base(self):
        str_base = self.base.strip()
        prefix = 1
        is_absorb_int = False

        for i, char in enumerate(str_base):
            if char in (I_SYMBOL, ' ', '+') or is_absorb_int:
                is_absorb_int = char in str(self.base)
                continue

            res_num = char
            if char in NUMBERS:
                j = 1

                if i + j == len(str_base):
                    self.postfix = prefix * int(res_num)
                    continue

                while str_base[i+j] in NUMBERS:
                    res_num += str_base[i+j]
                    j += 1

                    if i + j == len(str_base):
                        is_absorb_int = True
                        break

                    continue
                else:
                    self.base = int(res_num)
                    is_absorb_int = True
                    continue

            if res_num != char and char == I_SYMBOL:
                self.base = int(res_num)
                continue
            
            if char == '-':
                prefix = -1
                continue
            
            self.postfix = prefix * int(res_num)

    def __init__(self, base, postfix=None):
        """
        Simple class for operations with complex numbers.

        :param base: int 
        or string with format '{base}i +- {postfix}'
        examples: '10i - 15', '15i + 10'
        :param postfix: int
        """

        self.base = base
        self.postfix = postfix or 0

        if isinstance(base, str):
            self._parse_str_base()
    
    def __str__(self) -> str:

        if self.postfix:
            return f'{self.base}i + ({self.postfix})'
        return f'{self.base}i'
    
    def __add__(self, other):
        if isinstance(other, int):
            self.postfix += other
        elif isinstance(other, Complex):
            self.base += other.base
            self.postfix += other.postfix
        else:
            raise TypeError(f'unsupported operand type(s) for "{self}" and "{other}"')
        return self
    
    def __sub__(self, other):
        if isinstance(other, int):
            return self.__add__(-other)
        elif isinstance(other, Complex):
            other = copy(other)
            other.base = -other.base
            other.postfix = -other.postfix
            return self.__add__(other)
        else:
            raise TypeError(f'unsupported operand type(s) for "{self}" and "{other}"')
    
    def __mul__(self, other):
        if isinstance(other, int):
            if other == 0: return 0
            self.base *= other
            self.postfix *= other
            return self
        elif isinstance(other, Complex):

            if not self.postfix and not other.postfix:
                return self.base * other.base * -1

            new = copy(self)
            new.base = self.base * other.postfix + self.postfix * other.base
            new.postfix = -1 * (self.base * other.base) + self.postfix * other.postfix
            return new
        else:
            raise TypeError(f'unsupported operand type(s) for "{self}" and "{other}"')
